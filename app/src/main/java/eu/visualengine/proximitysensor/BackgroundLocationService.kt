package eu.visualengine.proximitysensor

import android.Manifest
import android.app.AlarmManager
import android.app.PendingIntent
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.widget.Toast
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import android.os.IBinder
import android.os.SystemClock
import android.util.Log
import android.location.LocationManager
import android.os.Bundle


class BackgroundLocationService : BroadcastReceiver() {


    override fun onReceive(context: Context?, intent: Intent?) {
        if (context != null) {
            val tokenStore = TokenStore(context)
            val remoteApi = RemoteApi(context)

            val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            val criteria = Criteria()
            criteria.horizontalAccuracy = Criteria.ACCURACY_HIGH

            val locator = object : LocationListener {
                override fun onLocationChanged(location: Location) {
                    Log.d("SERVICE", "Got location")
                    locationManager.removeUpdates(this)
                    val lat = location.latitude
                    val lon = location.longitude
                    Thread(Runnable {
                        remoteApi.updateLocation(tokenStore.getApiKey(), lat, lon)
                        Log.d("SERVICE", "Saved locator")
                    }).start()
                }

                override fun onStatusChanged(s: String, i: Int, bundle: Bundle) {

                }

                override fun onProviderEnabled(s: String) {

                }

                override fun onProviderDisabled(s: String) {

                }
            }
            if (context.getPackageManager().checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, context.getPackageName()) == PackageManager.PERMISSION_GRANTED) {
                locationManager.requestLocationUpdates( locationManager.getBestProvider(criteria, true), 1, 0.0f, locator)
                Log.d("SERVICE", "Registered locator")
            }
            Thread(Runnable {

                val apiKey = tokenStore.getApiKey()
                val systemStatus = remoteApi.isSystemArmed(apiKey)

                if(systemStatus != RemoteApi.SystemStatusCheckResult.NotArmed) {

                    val homeDistance = remoteApi.getHomeDistance(apiKey)

                    val timeTravel = homeDistance / 27.0;
                    Log.d("SERVICE", "Time travel " + timeTravel.toString())

                    val interval = ((maxOf(5.0, timeTravel * 0.5)) * 1000).toInt()

                    Log.d("SERVICE", "Time interval " + interval.toString())

                    val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
                    val intent = Intent(context, BackgroundLocationService::class.java)
                    val pendingintent = PendingIntent.getBroadcast(context, 0, intent, 0)

                    alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                            SystemClock.elapsedRealtime() +
                                    interval, pendingintent)

                }
            }).start();


        }
    }


}