package eu.visualengine.proximitysensor

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_setup.*

class SetupActivity : AppCompatActivity() {

    private val tokenStore = TokenStore(this)
    private val remoteApi = RemoteApi(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setup)
        apiKeyEditText.setText(tokenStore.getApiKey())
        saveButton.setOnClickListener {
            val thread = Thread(Runnable {
                val apiKey = apiKeyEditText.text.toString()
                val validationResult = remoteApi.checkApiKeyValidity(apiKey)
                var toastInfoString = ""
                if (validationResult == RemoteApi.ValidationResult.Valid){
                    toastInfoString = "API Key valid and saved!"
                }
                if (validationResult == RemoteApi.ValidationResult.NotValid){
                    toastInfoString = "API Key is not valid"
                }
                if (validationResult == RemoteApi.ValidationResult.Error){
                    toastInfoString = "Error occurred - cannot check the validity"
                }
                if(validationResult == RemoteApi.ValidationResult.Valid){
                    tokenStore.setApiKey(apiKey)
                }
                this.runOnUiThread(Runnable {
                    Toast.makeText(this, toastInfoString, Toast.LENGTH_SHORT).show()
                })
            }).start()
        }
    }


}
