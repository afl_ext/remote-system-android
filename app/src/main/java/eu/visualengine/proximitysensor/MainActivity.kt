package eu.visualengine.proximitysensor

import android.Manifest
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import kotlinx.android.synthetic.main.activity_main.*
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.widget.EditText
import android.content.Intent
import android.content.pm.PackageManager
import android.os.SystemClock
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log


class MainActivity : AppCompatActivity() {

    lateinit var updatingThread: Thread
    var updatingThreadShouldExit: Boolean = false

    private val tokenStore = TokenStore(this)
    private val remoteApi = RemoteApi(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        if (ContextCompat.checkSelfPermission(this,

                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    1)
        }

        settingsButton.setOnClickListener {
            startActivity(Intent(this, SetupActivity::class.java))
        }

        armingButton.setOnClickListener {
            Thread(Runnable {
                var result = remoteApi.toggleSystemArm(tokenStore.getApiKey())
                updateTextViews()
                if(result == RemoteApi.SystemStatusCheckResult.Armed){
                    scheduleLocationUpdate()
                }
            }).start()
        }

        startViewsUpdatingThread()
        Thread(Runnable {
            val systemStatus = remoteApi.isSystemArmed(tokenStore.getApiKey())
            if(systemStatus == RemoteApi.SystemStatusCheckResult.Armed){
                scheduleLocationUpdate()
            }
        }).start()
    }

    fun startViewsUpdatingThread(){

        updatingThread = Thread(Runnable {
            while (true) {
                updateTextViews()
                if (updatingThreadShouldExit) {
                    updatingThreadShouldExit = false
                    break
                }
                Thread.sleep(3000)
            }

        })
        updatingThread.start()
    }

    fun scheduleLocationUpdate(){
        var alarmManager = this.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        var intent = Intent(this, BackgroundLocationService::class.java)
        var pendingintent = PendingIntent.getBroadcast(this, 0, intent, 0)

        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() +
                        1000, pendingintent)
        Log.d("SERVICE", "SERIVE SHECULED")
    }

    override fun onPause() {
        super.onPause()
        updatingThreadShouldExit = true
    }

    override fun onResume() {
        super.onResume()
        try{
            if(!updatingThread.isAlive) {
                startViewsUpdatingThread()
            }
        } catch (e: IllegalThreadStateException){}
    }


    fun updateTextViews() {
        val apiKey = tokenStore.getApiKey()
        val systemStatus = remoteApi.isSystemArmed(apiKey)
        val homeDistance = remoteApi.getHomeDistance(apiKey)
        val lastUpdateString = remoteApi.getLastLocationUpdateString()
        this.runOnUiThread {
            statusTextView.text =
                    if (systemStatus == RemoteApi.SystemStatusCheckResult.Armed)
                        "Armed"
                    else if (systemStatus == RemoteApi.SystemStatusCheckResult.NotArmed)
                        "Not armed"
                    else
                        "Error occurred"
            distanceTextView.text = homeDistance.toString()
            lastBroadcastTextView.text = lastUpdateString
            Log.d("updating draws", lastUpdateString)
        }
    }
}
