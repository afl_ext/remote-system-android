package eu.visualengine.proximitysensor

import android.content.Context
import android.util.Log
import org.json.JSONObject
import org.json.JSONException
import java.io.BufferedReader
import java.io.BufferedWriter
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import java.text.DateFormat
import java.util.*
import javax.net.ssl.HttpsURLConnection


class RemoteApi (val context: Context) {

    enum class ValidationResult {
        Valid, NotValid, Error
    }

    enum class SystemStatusCheckResult {
        Armed, NotArmed, Error
    }

    private val apiEndpoint = "https://visualengine.eu/remote/api/public/"

    fun checkApiKeyValidity(apiKey: String): ValidationResult {
        val requestData = JSONObject()
        requestData.put("apiKey", apiKey)
        val responseData = performCall("validate-api-key", requestData)
        if (responseData.has("result")) {
            val resultBoolean = responseData.getBoolean("result")
            return if (resultBoolean)
                ValidationResult.Valid
            else
                ValidationResult.NotValid
        }
        return ValidationResult.Error
    }

    fun isSystemArmed(apiKey: String): SystemStatusCheckResult {
        val requestData = JSONObject()
        requestData.put("apiKey", apiKey)
        val responseData = performCall("is-system-armed", requestData)
        if (responseData.has("result")) {
            val resultBoolean = responseData.getBoolean("result")
            return if (resultBoolean)
                SystemStatusCheckResult.Armed
            else
                SystemStatusCheckResult.NotArmed
        }
        return SystemStatusCheckResult.Error
    }

    fun setSystemArmStatus(apiKey: String, status: Boolean): SystemStatusCheckResult {
        val requestData = JSONObject()
        requestData.put("apiKey", apiKey)
        requestData.put("status", status)
        val responseData = performCall("set-system-armed", requestData)
        if (responseData.has("result")) {
            val resultBoolean = responseData.getBoolean("result")
            return if (resultBoolean)
                SystemStatusCheckResult.Armed
            else
                SystemStatusCheckResult.NotArmed
        }
        return SystemStatusCheckResult.Error
    }

    fun toggleSystemArm(apiKey: String): SystemStatusCheckResult {
        val systemStatus = isSystemArmed(apiKey)
        if (systemStatus == SystemStatusCheckResult.Armed) {
            return setSystemArmStatus(apiKey, false)
        } else if (systemStatus == SystemStatusCheckResult.NotArmed) {
            return setSystemArmStatus(apiKey, true)
        }
        return SystemStatusCheckResult.Error
    }


    fun updateLocation(apiKey: String, lat: Double, lon: Double): Double {
        val requestData = JSONObject()
        requestData.put("apiKey", apiKey)
        requestData.put("lat", lat)
        requestData.put("lon", lon)
        val responseData = performCall("update-location", requestData)
        if (responseData.has("result")) {
            val date = Date()
            val stringDate = DateFormat.getDateTimeInstance().format(date)
            setLastLocationUpdateString(stringDate)
            return responseData.getDouble("result")
        }
        return 0.0
    }

    fun getLastLocationUpdateString(): String {
        val preferences = context.getSharedPreferences("lastLocationUpdateString", Context.MODE_PRIVATE)
        return preferences.getString("lastLocationUpdateString", "")
    }

    fun setLastLocationUpdateString(key: String) {
        val preferences = context.getSharedPreferences("lastLocationUpdateString", Context.MODE_PRIVATE)
        val editor = preferences.edit()
        editor.putString("lastLocationUpdateString", key)
        editor.commit()
    }

    fun getHomeDistance(apiKey: String): Double {
        val requestData = JSONObject()
        requestData.put("apiKey", apiKey)
        val responseData = performCall("get-home-distance", requestData)
        if (responseData.has("result")) {
            return responseData.getDouble("result")
        }
        return 0.0
    }

    private fun performCall(path: String, parameters: JSONObject): JSONObject {
        val url: URL
        var response = ""
        try {
            url = URL(apiEndpoint + path)

            val conn = url.openConnection() as HttpURLConnection
            conn.readTimeout = 5000
            conn.connectTimeout = 5000
            conn.doInput = true
            conn.doOutput = true


            val os = conn.outputStream
            val writer = BufferedWriter(
                    OutputStreamWriter(os, "UTF-8"))
            writer.write(parameters.toString())

            writer.flush()
            writer.close()
            os.close()
            val responseCode = conn.responseCode

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                var line: String?
                val br = BufferedReader(InputStreamReader(conn.inputStream))
                do {
                    line = br.readLine()
                    if (line == null) break
                    response += line
                } while (line != null)
            } else {
                response = ""
            }
            return JSONObject(response)
        } catch (e: JSONException) {
            e.printStackTrace()
            return JSONObject("{\"error\":\"JSON Exception ${e.message?.replace("\"", "'")}\"}")
        } catch (e: Exception) {
            e.printStackTrace()
            return JSONObject("{\"error\":\"Exception ${e.message?.replace("\"", "'")}\"}")
        }


    }
}