package eu.visualengine.proximitysensor

import android.content.Context
import android.content.Context.MODE_PRIVATE

class TokenStore(val context: Context) {

    fun getApiKey(): String {
        val preferences = context.getSharedPreferences("apiKey", MODE_PRIVATE)
        return preferences.getString("apiKey", "")
    }

    fun setApiKey(key: String) {
        val preferences = context.getSharedPreferences("apiKey", MODE_PRIVATE)
        val editor = preferences.edit()
        editor.putString("apiKey", key)
        editor.commit()
    }
}